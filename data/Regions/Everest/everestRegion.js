import { everestBaseCampBaseCampData } from "./places/everest_base_camp";

export const everestRegionData = {
  image: "image.jpg",
  time: "14 Days",
  place: [everestBaseCampBaseCampData],
};
