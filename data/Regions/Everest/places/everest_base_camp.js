export const everestBaseCampBaseCampData = {
  placeName: "EVEREST BASE CAMP AND KALA PATTHAR",
  image: "images.png",
  overView: [
    "The Everest Base Camp Trek is a challenge, 14 days of walking, high altitudes, rocky tracks, and some steep inclines, but what an achievement to say you reached the base camp of the highest mountain in the world. The path begins in ancient Kathmandu, where you'll acclimate and explore the city at your leisure while expecting your ascent. Your trek to Everest Base Camp world's tallest mountain will bring you over suspension bridges spanning chasms of thin air, through hidden Buddhist monasteries, and into the heart of the warm, rugged Sherpa culture.",
  ],
  itenary: [
    {
      heading: "Day 1 Arrive Kathmandu (1,300m/4,264ft)",
      body: "We will welcome you at the airport and drop you to the hotel.",
    },
    {
      heading:
        "Day 2 Fly Kathmandu—Lukla and trek to Phakding (2,562m /8700ft): 3-4 hrs trek",
      body: "Early morning, fly to Lukla for 35 minutes. The trail starts along the side of the Dudh Koshi valley before ascending to Ghat Trail climbs gently again about an hour to final destination Phakding",
    },
    {
      heading:
        "Day 3 Phakding - Namche Bazaar (3,440 m/11,280 ft): 10km, 6-7 hrs",
      body: "Trek leads through a beautiful pine forest, along Dudh Koshi River crossing many suspension bridges via Sherpa villages Benkar, Chimoa, Monjo and Jorsole national park permit checkpoint with the tranquility view of glistering Mt. Thamserku.",
    },
    {
      heading: "Day 4 Namche Bazaar: Acclimatization and exploration day",
      body: "Today will be spent resting and allowing your bodies to become acclimatized to the lofty altitude. There’s an optional 2 hour hike to Everest View Point, an uphill walk that will help speed acclimatization. Otherwise, spend the day exploring Namche Bazaar.",
    },
    {
      heading:
        "Day 5 Namche Bazzar - Tengboche (3,870m/12,694ft): 5-6 hrs. 10 km",
      body: "Breathtaking views of the Himalayas - Everest, Nuptse, Lhotse, Ama Dablam, Thamserku and Kwangde - are some of the highlights of the day.",
    },
    {
      heading: "Day 6 Trek from Tengboche to Pheriche (4312m): 5-6 hours",
      body: "The trail starts with a steep descent through woods towards a suspension bridge over the Imja Khola River. Then you begin a steady climb up to Pheriche via Pangboche, which houses the most ancient monastery in the region.",
    },
    {
      heading:
        "Day 7 Acclimatisation day in Pheriche (4312m) - optional side trek",
      body: "As it is important to ascend slowly you'll spend another day acclimatizing. There are many day hikes you can take from here, or you can simply have a day of rest.",
    },
    {
      heading: "Day 8 Trek from Pheriche to Lobuche (4920m): 4-5 hours",
      body: "The trail continues up the wide valley beneath the impressive peaks of Cholatse (6440m) and Tawoche (6542m). Afterward, the climb gets steeper to reach the foot of the Khumbu glacier.",
    },
    {
      heading:
        "Day 9 Lobuche - Gorak Shep (5,160 m/17,000ft) - Everest Base Camp (5,364 m/17,594 ft) Gorak Shep : 15km, 8-10 hrs",
      body: "On the way to our destination, we’ll approach the famed Khumbu Glacier and icefall, located on the slopes of Everest.",
    },
    {
      heading:
        "Day 10 Gorak Shep - Kala Patthar (5,545 m/18,192 ft) - Pheriche (4,280 m/14,070 ft), 15km, 8-10 hrs",
      body: "A very early morning climb of 2-3 hours up Kala Patthar offers a sunrise awakening of the Himalayan chain. First Everest then its surrounding vassals: Pumori, Lingtren, Khumbutse, Changtse, Lhotse and Nuptse.Plus Cho Oyo in the west and Ama Dablam to the south.",
    },
    {
      heading:
        "Day 11 Pheriche – Namche Bazaar (3,440 m/11,280 ft), 15km, 8-9 hrs",
      body: "Today a long descent leads to Phunki Thenga on the Imja Khola. The path then climbs to Sanasa beyond which it broadens for the pleasant contour walk back to Namche Bazaar.",
    },
    {
      heading: "Day 12 Namche Bazaar – Lukla (2,642m/8,668ft), 18km, 7-8 hrs",
      body: "From Namche, the trail descends to more comfortable altitudes, passing through the friendly farming villages, crossing the suspension bridge at Thado Koshi before rising to Ghat and Cheplung.",
    },
    {
      heading: "Day 13 Lukla – Kathmandu (1,300m/4,264ft), 35 min flight",
      body: "After an early breakfast, take the return flight to Kathmandu. Transfer to your hotel.",
    },
    {
      heading: "Day 14 Final Trek departure",
      body: "After breakfast, transfer to international airport and fly to onward destination.",
    },
    {
      heading: "",
      body: "",
    },
  ],
};
