import { Center, Flex, Text } from "@chakra-ui/react";
import ReccomendedCard from "./ReccomendedCard";
import TeamCard from "./TeamCard";

const OurTeamSection = () => {
  const items = new Array(3).fill("shiv");
  return (
    <Center
      w="full"
      display={"flex"}
      justifyContent="center"
      flexDir={"column"}
      alignItems="center"
    >
      <Text>Our Team</Text>
      <Flex w="80%" justify="space-evenly">
        {items.map((el, i) => (
          <TeamCard key={i} />
        ))}
      </Flex>
    </Center>
  );
};

export default OurTeamSection;
