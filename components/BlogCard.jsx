import { Avatar, Flex, Stack, Text } from "@chakra-ui/react";
import Image from "next/image";
import React from "react";
import {
  FaFacebook,
  FaFacebookF,
  FaInstagram,
  FaLinkedinIn,
  FaTag,
  FaTags,
  FaTwitter,
} from "react-icons/fa";
import { bannerImages } from "../constants/Images";

const BlogCard = () => {
  const socialClass = {
    fontSize: "2rem",
    ml: "3rem",
  };
  const socialMedia = [
    {
      link: "#",
      icon: <FaFacebookF />,
    },
    {
      link: "#",
      icon: <FaLinkedinIn />,
    },
    {
      link: "#",
      icon: <FaInstagram />,
    },
  ];

  return (
    <Flex
      align="center"
      justify="center"
      mt="2.9rem"
      direction={{ md: "column", base: "column", lg: "row" }}
      p="3"
    >
      <Flex pr={{ lg: "2rem", base: 0 }} position="relative" justify="center">
        {" "}
        <Image
          src={bannerImages[1]}
          alt="blog"
          width={1800}
          height={900}
          objectFit="fill"
        />
      </Flex>

      <Flex direction="column" justify="start">
        <Text fontSize="1.4rem" fontWeight="bold" mb="5" color="gray.700">
          A Week in Mustang
        </Text>
        <Flex mb="1rem" color="gray.400">
          <Text>19th Feb, 2033 </Text>
          <Flex align="center" ml="3rem">
            {" "}
            <FaTags />
            <Text ml=".5rem"> Adventure </Text>
          </Flex>
        </Flex>
        <Text color="gray.700">
          Lorem ipsum dolor sit amet mattis. Class ante erat. Dapibus ipsum
          turpis libero sagittis suspendisse. Velit fringilla a. A lorem ipsum
          vel urna aptent. Consectetuer urna ante elementum maecenas magnis
          sodales mauris pede. Quo venenatis volutpat aliquam ut libero. Dui
          urna pede velit...
        </Text>
        <Flex justify="start" mt="2rem" align="center">
          {" "}
          <Flex>
            {" "}
            <Avatar h={6} w={6} />
            <Text ml="1rem" fontWeight="semibold" color="gray.600">
              {" "}
              Shiv Raj{" "}
            </Text>
          </Flex>
          <Flex ml="4rem">
            {socialMedia?.map((el, i) => (
              <Flex
                key={i}
                p="2"
                m="1"
                borderRadius="full"
                transitionDuration="2s"
                _hover={{
                  bgColor: "gray",
                }}
              >
                {" "}
                {el?.icon}{" "}
              </Flex>
            ))}{" "}
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default BlogCard;
