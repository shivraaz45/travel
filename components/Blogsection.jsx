import { Center, Text } from "@chakra-ui/react";
import { domAnimation, LazyMotion, m } from "framer-motion";
import Image from "next/image";
import { Lazy } from "swiper";
import { bannerImages } from "../constants/Images";
import BlogList from "../components/BlogList";
import { useInView } from "react-intersection-observer";

const Blogsection = () => {
  const items = new Array(5).fill("shiv");

  return (
    <LazyMotion features={domAnimation}>
      <section className="text-gray-600 body-font overflow-hidden">
        <div className="container px-2 lg:px-5  py-2 mx-auto ">
          <h1 className="font-extrabold py-16 text-4xl">Blogs</h1>
          <div className="-my-8 divide-y-2 divide-gray-100">
            <>
              {" "}
              {items.map((el, i) => (
                <BlogList key={i} i={i} />
              ))}
            </>
          </div>
        </div>
      </section>
    </LazyMotion>
  );
};

export default Blogsection;
