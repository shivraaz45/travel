import { domAnimation, LazyMotion, m } from "framer-motion";
import Link from "next/link";
import { useInView } from "react-intersection-observer";

const BlogList = ({ i }) => {
  const { ref, inView, entry } = useInView({
    /* Optional options */
    threshold: 0,
    triggerOnce: true,
  });
  return (
    <LazyMotion features={domAnimation}>
      <div ref={ref} className="py-8 flex flex-wrap md:flex-nowrap">
        {inView && (
          <m.div
            initial={{ opacity: 0, y: -60 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: 60 }}
            transition={{
              duration: 1,
            }}
            key={i}
          >
            <div className="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col  ">
              <span className="font-semibold title-font text-gray-700">
                CATEGORY
              </span>
              <span className="mt-1 text-gray-500 text-sm">12 Jun 2019</span>
            </div>
            <div className="md:flex-grow">
              <h2 className="text-2xl font-medium text-gray-900 title-font mb-2">
                Bitters hashtag waistcoat fashion axe chia unicorn
              </h2>
              <p className="leading-relaxed">
                Glossier echo park pug, church-key sartorial biodiesel
                vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon
                party messenger bag selfies, poke vaporware kombucha
                lumbersexual pork belly polaroid hoodie portland craft beer.
              </p>
              <Link href={`/blogs/${i + 1}`}>
                <a className="text-indigo-500 inline-flex items-center mt-4">
                  read More
                  <svg
                    className="w-4 h-4 ml-2"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="2"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path d="M5 12h14"></path>
                    <path d="M12 5l7 7-7 7"></path>
                  </svg>
                </a>
              </Link>
            </div>
          </m.div>
        )}
      </div>
    </LazyMotion>
  );
};

export default BlogList;
