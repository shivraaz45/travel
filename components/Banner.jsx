import { Flex } from "@chakra-ui/react";
import {
  domAnimation,
  LazyMotion,
  m,
  useScroll,
  useTransform,
} from "framer-motion";
import { bannerImages } from "../constants/Images";
import { useInView } from "react-intersection-observer";
import BannerHeading from "./BannerHeading";

const Banner = ({ image, topic, minH = "80vh", children }) => {
  const { scrollY } = useScroll();

  const y2 = useTransform(scrollY, [0, 300], [0, -500]);

  return (
    <LazyMotion features={domAnimation}>
      <m.div className="w-full ">
        <Flex
          backgroundImage={image}
          minH={minH}
          h="100%"
          bgPosition="center"
          bgSize="cover"
          justify="center"
          align="center"
        >
          {topic && <BannerHeading text={topic} />}
          {children && children}
        </Flex>
      </m.div>
    </LazyMotion>
  );
};

export default Banner;
