import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  EffectFade,
  Autoplay,
} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/effect-fade";
import { Swiper, SwiperSlide } from "swiper/react";

const CSwiper = ({ children }) => {
  return (
    <Swiper
      className="w-full h-full center bg-green-600  "
      // install Swiper modules
      spaceBetween={50}
      slidesPerView={1}
      centeredSlides={true}
      effect="fade"
      loop={true}
      autoplay={{
        delay: 10 * 1000,
        disableOnInteraction: false,
      }}
      onSwiper={(swiper) => console.log(swiper)}
      onSlideChange={() => console.log("slide change")}
      modules={[Autoplay, EffectFade, Navigation, Pagination]}
    >
      {bannerImages.map((image, i) => (
        <SwiperSlide key={i}></SwiperSlide>
      ))}
      ...
    </Swiper>
  );
};

export default CSwiper;
