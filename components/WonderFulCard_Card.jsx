import Image from "next/image";
import { bannerImages } from "../constants/Images";
import {
  AnimatePresence,
  domAnimation,
  LazyMotion,
  m,
  motion,
} from "framer-motion";
import { Flex, Text, useBoolean } from "@chakra-ui/react";
import { AiOutlineSmile } from "react-icons/ai";
import { MdOutlineReplay, MdPersonOutline } from "react-icons/md";
import { TimeIcon } from "@chakra-ui/icons";
const WonderFulCard_Card = () => {
  const [flag, setFlag] = useBoolean();
  return (
    <div
      className="wonder w-full lg:w-[20rem] md:w-1/2   bg-white border-#ebebeb border-2"
      onMouseEnter={setFlag.toggle}
      onMouseLeave={setFlag.toggle}
    >
      <div className="  block relative h-72  overflow-hidden">
        <Image
          layout="fill"
          alt="ecommerce"
          className="object-cover object-center w-full h-full block"
          src={bannerImages[0]}
        />
        <AnimatePresence>
          {flag && (
            <motion.div
              key="overlay"
              initial={{ opacity: 0, bottom: "-10%" }}
              animate={{ opacity: 1, bottom: "0%" }}
              exit={{ opacity: 0, bottom: "-80%" }}
              transition={{ duration: 0.5 }}
              className="p-4 flex flex-col absolute w-full h-full bg-opacity-5  backdrop-blur-sm  "
            >
              <LazyMotion features={domAnimation}>
                <div className="flex justify-between items-center ">
                  <m.h4
                    initial={{ opacity: 0, y: 20 }}
                    animate={{ opacity: 1, y: 0 }}
                    exit={{ opacity: 0, y: -20 }}
                    transition={{ duration: 0.5 }}
                    className=" font-extrabold text-white"
                  >
                    Taste of KTM
                  </m.h4>
                  <m.h1
                    initial={{ opacity: 0, y: 20 }}
                    animate={{ opacity: 1, y: 0 }}
                    exit={{ opacity: 0, y: -20 }}
                    transition={{ duration: 0.7 }}
                    className="font-extrabold  text-xl text-[#40c1b9] "
                  >
                    {" "}
                    $500
                  </m.h1>
                </div>
                <Flex pt={2} align="center" textColor="white">
                  <AiOutlineSmile />
                  <Text className="text-white ml-2"> 9.5</Text>

                  <Text className="text-white ml-1"> Superb</Text>
                </Flex>
                <Flex py={4} color="white">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit.
                </Flex>
                <Flex color="white">
                  <Flex align="center">
                    {" "}
                    <TimeIcon size={20} />
                    <Text ml={1}>10 days</Text>
                  </Flex>
                  <Flex ml={6} align="center">
                    <MdPersonOutline size={25} />
                    <Text ml={1}>12+</Text>
                  </Flex>
                </Flex>
              </LazyMotion>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </div>
  );
};

export default WonderFulCard_Card;
