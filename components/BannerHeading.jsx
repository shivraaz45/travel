import { Text } from "@chakra-ui/react";
import React from "react";

const BannerHeading = ({ text }) => {
  return (
    <Text
      fontSize={{ base: "2xl", lg: "5xl", md: "3xl" }}
      fontWeight="bold"
      color="white"
    >
      {text}
    </Text>
  );
};

export default BannerHeading;
