import { Box, Container, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { AiOutlineSetting } from "react-icons/ai";
import { bannerImages } from "../constants/Images";

const WhyChooseusSection = () => {
  const single = {
    title: "Lorem",
    icon: <AiOutlineSetting fontSize={"5rem"} fontWeight="100" />,
    desc: "Lorem ipsum dolor sit amet mattis. Class ante erat. Dapibus ipsum turpis libero sagittis suspen disse. Velit fringilla a. A lorem ipsum vel urna aptent.",
  };
  const items = new Array(6).fill(single);
  return (
    <Box
      w="full"
      h="full"
      display="flex"
      justifyContent="center"
      flexWrap="wrap"
      alignItems="center"
      bgImage={bannerImages[0]}
      bgSize="cover"
      bgPosition={"center"}
      bgAttachment="fixed"
      position={"relative"}
      backdropBrightness={80}
      color="white"
      py="2rem"
      _before={{
        content: "''",
        position: "absolute",
        w: "full",
        h: "full",
        opacity: 0.5,
      }}
    >
      <Flex
        direction="column"
        w={{ lg: "80%", md: "90%", base: "full" }}
        justify="center"
        align="center"
        backdropFilter="auto"
        backdropBlur={"6px"}
        p={{ base: ".2rem", lg: "2rem" }}
        boxShadow="50px 50px 100px #9d9d9d,
             -50px -50px 100px #ffffff;"
      >
        <Text fontSize={"2.3rem"} fontWeight="extrabold" mb="3rem">
          Why Choose us?
        </Text>

        <SimpleGrid
          columns={{ lg: 3, md: 2, base: 1 }}
          row={2}
          spacing={5}
          w="full"
        >
          {items?.map((item, i) => (
            <Box key={i} height="auto">
              <Flex align="center">
                {" "}
                {item?.icon} <Text ml="1rem"> {item?.title}</Text>
              </Flex>
              <Container mt="1"> {item?.desc} </Container>
            </Box>
          ))}
        </SimpleGrid>
      </Flex>
    </Box>
  );
};

export default WhyChooseusSection;
