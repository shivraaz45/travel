import {
  Heading,
  Avatar,
  Box,
  Center,
  Image,
  Flex,
  Text,
  Stack,
  Button,
  useColorModeValue,
  HStack,
} from "@chakra-ui/react";
import { FaFacebookF, FaInstagram, FaLinkedinIn } from "react-icons/fa";

export default function TeamCard() {
  const socialMedia = [
    {
      link: "#",
      icon: <FaFacebookF />,
    },
    {
      link: "#",
      icon: <FaLinkedinIn />,
    },
    {
      link: "#",
      icon: <FaInstagram />,
    },
  ];

  return (
    <Center py={3}>
      <Box
        w={"full"}
        bg={useColorModeValue("white", "gray.800")}
        boxShadow={"md"}
        transitionDuration=".4s"
        overflow={"hidden"}
        _hover={{
          boxShadow: "2xl",
        }}
      >
        <Image
          h={"260px"}
          alt="Member"
          w={"full"}
          src={
            "https://images.unsplash.com/photo-1612865547334-09cb8cb455da?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
          }
          objectFit={"cover"}
        />

        <Box p={6}>
          <Stack spacing={0} align={"center"} mb={5}>
            <Heading fontSize={"2xl"} fontWeight={500} fontFamily={"body"}>
              John Doe
            </Heading>
            <Center>
              <Text color={"gray.500"} align="center">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy
              </Text>
            </Center>
          </Stack>
          <Center>
            {" "}
            {socialMedia?.map((el, i) => (
              <Flex
                key={i}
                p="2"
                m="1"
                borderRadius="full"
                transitionDuration="2s"
                _hover={{
                  bgColor: "gray",
                }}
              >
                {" "}
                {el?.icon}{" "}
              </Flex>
            ))}{" "}
          </Center>
        </Box>
      </Box>
    </Center>
  );
}
