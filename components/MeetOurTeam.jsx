import { Flex, SimpleGrid, Text, VStack } from "@chakra-ui/react";
import TeamCard from "./TeamCard";

const MeetOurTeam = () => {
  const items = new Array(3).fill("shiv");

  return (
    <Flex direction="column">
      <VStack>
        <Text fontSize="2.3rem">Meet Our Team</Text>
        <Text color="gray.500">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore
        </Text>
      </VStack>
      <SimpleGrid
        columns={{ base: 1, md: 2, lg: 3 }}
        spacing={{ base: 1, md: 3, lg: 10 }}
      >
        {items?.map((item, i) => (
          <TeamCard key={i} />
        ))}
      </SimpleGrid>
    </Flex>
  );
};

export default MeetOurTeam;
