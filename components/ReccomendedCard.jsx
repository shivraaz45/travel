import { Box, Flex, Text } from "@chakra-ui/react";
import { bannerImages } from "../constants/Images";

const ReccomendedCard = () => {
  return (
    <Flex
      height="300px"
      position="relative"
      justify="center"
      align="center"
      overflow="hidden"
      cursor="pointer"
      transitionDuration=".4s"
      _hover={{
        opacity: 0.7,
        backdropBlur: "4px",
      }}
    >
      <Box
        bgSize="cover"
        backgroundImage={bannerImages[0]}
        bgPosition="center"
        position="absolute"
        w="100%"
        h="100%"
        _hover={{
          filter: "grayscale(100%)",
        }}
      >
        <Flex
          className="bg-opacity-5  backdrop-blur-sm "
          position="absolute"
          p="2"
          bottom="0"
          w="full"
          h="fit-content"
          justifyContent="space-between"
          alignItems="end"
        >
          <Text fontSize={20} fontWeight="extrabold" color="white">
            Mustang
          </Text>
          <Text fontSize={20} fontWeight="extrabold" color="white">
            $400
          </Text>
        </Flex>
      </Box>
    </Flex>
  );
};

export default ReccomendedCard;
