import { Box, Flex, Text, useBoolean } from "@chakra-ui/react";
import { bannerImages } from "../constants/Images";

const DestinationCard = () => {
  const [flag, setFlag] = useBoolean();
  return (
    <Flex
      height="300px"
      position="relative"
      justify="center"
      align="center"
      overflow="hidden"
      cursor="pointer"
    >
      <Box
        className="scale-100 hover:scale-110 duration-1000"
        bgSize="cover"
        backgroundImage={bannerImages[0]}
        bgPosition="center"
        position="absolute"
        w="100%"
        h="100%"
      >
        <Flex
          position="absolute"
          w="full"
          h="full"
          justifyContent="center"
          alignItems="center"
        >
          <Text fontSize="2rem" fontWeight="extrabold" color="white">
            {" "}
            Paris{" "}
          </Text>
        </Flex>
      </Box>
    </Flex>
  );
};

export default DestinationCard;
