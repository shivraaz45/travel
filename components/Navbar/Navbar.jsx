import {
  Box,
  Flex,
  Text,
  IconButton,
  Stack,
  Collapse,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  useBoolean,
  Image,
} from "@chakra-ui/react";
import {
  HamburgerIcon,
  CloseIcon,
  ChevronDownIcon,
  ChevronRightIcon,
} from "@chakra-ui/icons";
import { useCallback, useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { LOGO } from "../../constants/Images";

export default function WithSubnavigation() {
  const { isOpen, onToggle } = useDisclosure();
  const [flag, setFlag] = useBoolean();
  const [scrolled, setScrolled] = useState(false);
  const router = useRouter();
  const handleScroll = useCallback(() => {
    setTimeout(() => {
      if (window.scrollY > 0) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    }, 2000);
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll]);
  const linkColor = useColorModeValue("white", "white");
  const linkHoverColor = useColorModeValue("white");
  const popoverContentBgColor = useColorModeValue("white", "gray.800");
  console.log(router.pathname);
  return (
    <Box
      position="fixed"
      zIndex={100}
      top={0}
      w="full"
      backdropFilter="auto"
      backdropBlur={scrolled ? "6px" : "0px"}
      background={scrolled ? "blackAlpha.600" : "transparent"}
      transitionDuration="1s"
    >
      <Flex
        backdropBlur={0.5}
        color={useColorModeValue("gray.600", "white")}
        minH={"60px"}
        py={{ base: 2 }}
        px={{ base: 4 }}
        align={"center"}
      >
        <IconButton
          position="absolute"
          display={{ lg: "none" }}
          _active={{
            bg: "none",
          }}
          _focus={{
            bg: "none",
          }}
          _blur={{
            bg: "none",
          }}
          bg="none"
          onClick={onToggle}
          icon={
            isOpen ? (
              <CloseIcon color="white" w={3} h={3} />
            ) : (
              <HamburgerIcon color="white" w={5} h={5} />
            )
          }
          aria-label={"Toggle Navigation"}
        />
        <Flex
          flex={{ base: 1 }}
          justify={{ base: "center", md: "center", lg: "space-around" }}
        >
          <Image
            src={LOGO}
            alt="logo"
            ml={{ base: 0, md: "4rem" }}
            h="full"
            w="10rem"
            objectFit="contain"
          />

          <Stack
            direction={{ base: "column", lg: "row" }}
            justify={{ base: "center", lg: "end" }}
            align="center"
            spacing={8}
            overflow="hidden"
            mr={{ lg: "7rem", base: "0" }}
            position={{ base: "absolute", md: "static", lg: "static" }}
            top="14"
            bg={{ base: "blackAlpha.700", lg: "transparent" }}
            w="full"
            transitionDuration="1s"
            h={{ base: `${isOpen ? "500px" : "0"}`, lg: "auto" }}
          >
            {NAV_ITEMS.map((navItem) => (
              <Box
                key={navItem.label}
                fontSize={"sm"}
                fontWeight="bold"
                color={linkColor}
                textTransform="uppercase"
                _hover={{
                  textDecoration: "none",
                  color: linkHoverColor,
                }}
                borderBottom="3px solid transparent"
                borderBottomColor={`${
                  router.pathname === navItem.href ? "orange" : "transpaent"
                }`}
              >
                <Link p={2} href={navItem.href ?? "#"} passHref>
                  {navItem.label}
                </Link>
              </Box>
            ))}
          </Stack>
        </Flex>
      </Flex>

      <Collapse in={isOpen} animateOpacity></Collapse>
    </Box>
  );
}

const NAV_ITEMS = [
  {
    label: "Home",
    href: "/",
  },
  {
    label: "Destinations",
    href: "/destinations",
  },
  {
    label: "About us",
    href: "/aboutus",
  },
  {
    label: "Contact us",
    href: "/contact",
  },
  {
    label: "FAQ",
    href: "/faq",
  },
  {
    label: "Blogs",
    href: "/blogs",
  },
];

// const NAV_ITEMS = [
//   {
//     label: "Inspiration",
//     children: [
//       {
//         label: "Explore Design Work",
//         subLabel: "Trending Design to inspire you",
//         href: "#",
//       },
//       {
//         label: "New & Noteworthy",
//         subLabel: "Up-and-coming Designers",
//         href: "#",
//       },
//     ],
//   },
//   {
//     label: "Find Work",
//     children: [
//       {
//         label: "Job Board",
//         subLabel: "Find your dream design job",
//         href: "#",
//       },
//       {
//         label: "Freelance Projects",
//         subLabel: "An exclusive list for contract work",
//         href: "#",
//       },
//     ],
//   },
//   {
//     label: "Learn Design",
//     href: "#",
//   },
//   {
//     label: "Hire Designers",
//     href: "#",
//   },
// ];
