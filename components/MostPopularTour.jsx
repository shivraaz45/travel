import { Box, Center, Flex, Text } from "@chakra-ui/react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  EffectFade,
  Autoplay,
} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/effect-fade";
import { Swiper, SwiperSlide } from "swiper/react";
import Card from "./Card";
import {
  AnimatePresence,
  motion,
  useScroll,
  useTransform,
} from "framer-motion";

const MostPopularTour = () => {
  const images = ["a", "a", "a"];
  const { scrollY } = useScroll();
  const y1 = useTransform(scrollY, [0, 300], [0, 200]);
  const y2 = useTransform(scrollY, [0, 500], [0, -500]);

  return (
    <AnimatePresence>
      <Flex
        position="relative"
        as={motion.div}
        // style={{ y: y2 }}
        justify="center"
        align="center"
        direction="column"
        py={{ base: "1rem", lg: "3rem" }}
        bg="#f9f9f9 !important"
      >
        {" "}
        <Text
          fontSize={{ base: "1.2rem", md: "2rem", lg: "3rem" }}
          align="center"
        >
          {" "}
          Most Popular Tour
        </Text>
        <Center flexDirection="column" color="gray.500" textAlign="center">
          {" "}
          <Box textAlign="center" mt="1rem">
            loremEst eiusmod culpa ipsum ex exercitation cillum
          </Box>
          <Box mt=".5rem">
            {" "}
            elit anim commodo duis. Ipsum incididunt qui proident culpa dolore
            commodo minim labore nulla ea amet nostrud.{" "}
          </Box>{" "}
        </Center>
        <Center w="fit-content">
          <Card />
        </Center>
      </Flex>
    </AnimatePresence>
  );
};

export default MostPopularTour;
