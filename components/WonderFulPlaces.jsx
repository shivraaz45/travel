import { Box, Center, Flex, Text } from "@chakra-ui/react";
import Card from "./Card";
import WonderfulCard from "./WonderfulCard";

const WonderFulPlaces = () => {
  return (
    <Flex
      justify="center"
      align="center"
      direction="column"
      py={{ base: "1rem", lg: "3rem" }}
      bg="white !important"
    >
      {" "}
      <Text fontSize={{ lg: "3rem", md: "2rem", sm: "1rem" }}>
        {" "}
        Wonderful places to visit
      </Text>
      <Center flexDirection="column" color="gray.500">
        {" "}
        <Box textAlign="center" mt="1rem">
          loremEst eiusmod culpa ipsum ex exercitation cillum
        </Box>
        <Box textAlign="center">
          {" "}
          elit anim commodo duis. Ipsum incididunt qui proident culpa dolore
          commodo minim labore nulla ea amet nostrud.{" "}
        </Box>{" "}
      </Center>
      <Center w="full">
        <WonderfulCard />
      </Center>
    </Flex>
  );
};

export default WonderFulPlaces;
