import {
  Box,
  Center,
  Container,
  Flex,
  useBoolean,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import { bannerImages } from "../constants/Images";
import WonderFulCard_Card from "./WonderFulCard_Card";

const WonderfulCard = () => {
  const [overLay, setOverLay] = useState(null);
  return (
    <section className="text-gray-600 body-font w-full lg:w-[79%] flex justify-center items-center ">
      <div className="container py-4 lg:py-14 lg:mx-12  ">
        <Flex wrap="wrap" justify="center" align="center">
          {new Array(8).fill("shiv").map((el, i) => (
            <Link key={i} href={`/places/${i + 1}`}>
              <Box as="a" w="auto">
                {" "}
                <WonderFulCard_Card key={i} />
              </Box>
            </Link>
          ))}
        </Flex>
      </div>
    </section>
  );
};

export default WonderfulCard;
