import { Box } from "@chakra-ui/react";
import { motion } from "framer-motion";
import SvgBg from "./SvgBg";

const OverView = ({ overView }) => {
  return (
    <Box sx={{ "::first-letter": { fontSize: "2em", color: "tomato" } }}>
      {overView?.map((el, i) => (
        <Box
          as={motion.div}
          initial={{ opacity: 0, y: 300 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: -300 }}
          transitionDuration=".5s"
          transitionTimingFunction="ease-out"
          key={i}
          mt="4"
          fontSize={{ base: "1rem", lg: "1.4rem" }}
        >
          {" "}
          {el}{" "}
        </Box>
      ))}
    </Box>
  );
};

export default OverView;
