import { Box, Center, Flex, keyframes, Text } from "@chakra-ui/react";
import { domAnimation, LazyMotion, m } from "framer-motion";

const Slider = ({ image, i }) => {
  return (
    <Flex
      justify="center"
      direction="column"
      align="center"
      h="full"
      bgImage={`url(${image})`}
      bgPosition="center"
      bgRepeat="no-repeat"
      bgSize="cover"
    >
      <Flex
        w="full"
        h="full"
        justify="center"
        align="center"
        fontWeight="bold"
        color="white"
        fontSize="6rem"
        _before={{
          content: "''",
          width: "full",
          height: "full",
          position: "absolute",
          bgGradient: "linear(to-b, black 0%, transparent)",
          opacity: 0.5,
          zIndex: -1000,
        }}
      >
        <LazyMotion features={domAnimation}>
          <Center
            as={m.div}
            initial={{ opacity: 0, scale: 0 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0 }}
            transitionDelay="1s"
            transitionDuration="3s"
            color="white"
          >
            {" "}
            Let us take you away{" "}
          </Center>
        </LazyMotion>
      </Flex>
    </Flex>
  );
};

export default Slider;
