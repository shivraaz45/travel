import Image from "next/image";
import { bannerImages } from "../constants/Images";
import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  EffectFade,
  Autoplay,
} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/effect-fade";
import { Swiper, SwiperSlide } from "swiper/react";
import { Flex, Text, useMediaQuery } from "@chakra-ui/react";
import { AiOutlineSmile } from "react-icons/ai";
import { MdOutlineReplay, MdPersonAdd, MdPersonOutline } from "react-icons/md";
import Link from "next/link";
const Card = () => {
  const [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");
  const [isLargerThan600] = useMediaQuery("(min-width: 700px)");

  return (
    <section className="text-gray-600 body-font  w-full lg:w-[75%] ">
      <div className="container  py-5 lg:py-14 lg:mx-12">
        <div className="flex ">
          <Swiper
            className="w-full  h-full flex justify-center items-center    "
            // install Swiper modules
            slidesPerView={isLargerThan1280 ? 3 : isLargerThan600 ? 2 : 1}
            spaceBetween={1}
            loop={true}
            navigation={true}
            autoplay={{
              delay: 6000,
            }}
            modules={[Pagination, Navigation, Autoplay]}
          >
            {new Array(5).fill("shiv").map((el, i) => (
              <SwiperSlide key={i} className="flex justify-center w-full">
                <Link key={i} href={`/places/${i + 1}`}>
                  <a>
                    {" "}
                    <div
                      className="lg:w-[30rem]   w-full bg-white border-#ebebeb border-2"
                      key={el}
                    >
                      <a className="block relative h-72  overflow-hidden">
                        <Image
                          layout="fill"
                          alt="ecommerce"
                          className="object-cover object-center w-full h-full block"
                          src={bannerImages[0]}
                        />
                      </a>
                      <div className="p-4 flex flex-col">
                        <div className="flex justify-between items-center ">
                          <h4 className=" font-extrabold text-black">
                            Taste of KTM
                          </h4>
                          <h1 className="font-extrabold  text-xl text-[#40c1b9] ">
                            {" "}
                            $500
                          </h1>
                        </div>
                        <Flex pt={2} align="center" textColor="#999">
                          <AiOutlineSmile />
                          <Text className="text-gray ml-2"> 9.5</Text>

                          <Text className="text-gray ml-1"> Superb</Text>
                        </Flex>
                        <Flex py={4}>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Lorem ipsum dolor sit amet, consectetur
                          adipiscing elit.
                        </Flex>
                        <Flex>
                          <Flex align="center">
                            {" "}
                            <MdOutlineReplay size={20} />
                            <Text ml={1}>10 days</Text>
                          </Flex>
                          <Flex ml={6} align="center">
                            <MdPersonOutline size={25} />
                            <Text ml={1}>12+</Text>
                          </Flex>
                        </Flex>
                      </div>
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default Card;
