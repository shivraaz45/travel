import { Box, Circle, Flex, Spacer, Text } from "@chakra-ui/react";
import {
  AnimatePresence,
  domAnimation,
  LazyMotion,
  m,
  motion,
} from "framer-motion";

const Itenary = ({ data }) => {
  let raw = [];
  for (const [key, value] of Object.entries(data)) {
    raw.push({ ...value, day: key });
  }
  return (
    <LazyMotion features={domAnimation}>
      <Flex
        id="over_view"
        key="over_view"
        as={motion.div}
        initial={{ opacity: 0, y: 300 }}
        animate={{ opacity: 1, y: 0 }}
        exit={{ opacity: 0, y: -300 }}
        transitionDuration=".5s"
        transitionTimingFunction="ease-out"
        direction="column"
        overflow="hidden"
      >
        {" "}
        {raw?.map((el, i) => (
          <Flex key={i} direction="column" position="relative" p="5">
            <Flex
              position="absolute"
              left="2"
              w="0.5"
              h="100%"
              bgGradient={"linear(to-b,lime,transparent)"}
              justify="center"
              _last={{
                overflow: "hidden",
              }}
            >
              <Circle
                bg="white"
                border="1px solid lime"
                w="3"
                h="3"
                right="50%"
              />
            </Flex>
            <Flex direction="column" px="4">
              <Flex justify="start">
                {" "}
                <Text
                  mr="1"
                  mt="-2"
                  color="tomato"
                  fontSize={{ base: "1rem", lg: "2rem" }}
                  fontWeight="extrabold"
                >
                  {" "}
                  Day {i + 1}{" "}
                </Text>{" "}
              </Flex>
              <Flex>
                {" "}
                <Text
                  align={"start"}
                  className="first-letter:text-[1.5rem]  first-letter:font-bold"
                  color="	#585858"
                  fontWeight="medium"
                  fontSize={{ base: "1rem", lg: "1.4rem" }}
                >
                  {el?.desc}{" "}
                </Text>
              </Flex>
            </Flex>
          </Flex>
        ))}{" "}
      </Flex>
    </LazyMotion>
  );
};

export default Itenary;
