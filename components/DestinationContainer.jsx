import { Box, Flex, Grid, SimpleGrid } from "@chakra-ui/react";
import Link from "next/link";
import { bannerImages } from "../constants/Images";
import DestinationCard from "./DestinationCard";

const DestinationContainer = () => {
  const items = new Array(8).fill("shiv");

  return (
    <SimpleGrid
      mt={{ base: "1rem", lg: "3rem" }}
      w={{ base: "full", md: "full", lg: "80%" }}
      p={[0, 0, "2rem"]}
      columns={[1, 2, 3]}
      row={[2, null, 5]}
      spacing="10px"
    >
      {items?.map((el, i) => (
        <Link key={i} href={`/places/${i + 1}`}>
          <a>
            {" "}
            <Link href={`/places/${i + 1}`}>
              {" "}
              <a>
                {" "}
                <DestinationCard key={i} />
              </a>
            </Link>
          </a>
        </Link>
      ))}
    </SimpleGrid>
  );
};

export default DestinationContainer;
