import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Text,
} from "@chakra-ui/react";

const FAQAccordin = ({ item }) => {
  return (
    <Accordion allowToggle allowMultiple transitionDuration={"4s"}>
      <AccordionItem>
        {({ isExpanded }) => (
          <>
            <h2>
              <AccordionButton outline="none">
                <Box
                  flex="1"
                  textAlign="left"
                  py={4}
                  _hover={{
                    color: "red",
                  }}
                >
                  <Text color={isExpanded && "red"}> {item?.question} </Text>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>{item?.answer}</AccordionPanel>
          </>
        )}
      </AccordionItem>
    </Accordion>
  );
};

export default FAQAccordin;
