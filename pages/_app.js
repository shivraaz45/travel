import "../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import { LazyMotion } from "framer-motion";
import dynamic from "next/dynamic";
import NextNProgress from "nextjs-progressbar";

const AnimatedCursor = dynamic(() => import("react-animated-cursor"), {
  ssr: false,
});
function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <Component {...pageProps} />
      {/* <NextNProgress stopDelayMs={0} /> */}
      {/* <AnimatedCursor
        innerSize={15}
        outerSize={10}
        color="120,190,33"
        clickables={[
          "a",
          'input[type="text"]',
          'input[type="email"]',
          'input[type="number"]',
          'input[type="submit"]',
          'input[type="image"]',
          "label[for]",
          "select",
          "textarea",
          "button",
          ".link",
        ]}
      /> */}
    </ChakraProvider>
  );
}

export default MyApp;
