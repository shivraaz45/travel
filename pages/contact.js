import Banner from "../components/Banner";
import { bannerImages } from "../constants/Images";
import Navbar from "../components/Navbar/Navbar";
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Text,
  Textarea,
  useToast,
  VStack,
} from "@chakra-ui/react";
import emailjs from "@emailjs/browser";
import {
  FaFacebook,
  FaInstagram,
  FaPersonBooth,
  FaTwitter,
} from "react-icons/fa";

import { EmailIcon } from "@chakra-ui/icons";
import Footer from "../components/Footer";
import { useRouter } from "next/router";
import { useRef } from "react";

export default function Contact() {
  const socialLinks = [
    {
      link: "#",
      icon: <FaFacebook fontSize="3rem" />,
    },
    {
      link: "#",
      icon: <FaInstagram fontSize="3rem" />,
    },
    {
      link: "#",
      icon: <FaTwitter fontSize="3rem" />,
    },
  ];
  const toast = useToast();

  const scrollToContactForm = () => {
    const contact = document.getElementById("contact_form");
    contact.scrollIntoView({ behavior: "smooth", block: "start" });
  };
  const scrollToMap = () => {
    const location = document.getElementById("location");
    location.scrollIntoView({ behavior: "smooth", block: "start" });
  };
  const ref = useRef();

  const sendEmail = (e) => {
    e.preventDefault();
    let templateParams = {
      name: "James",
      notes: "Check this out!",
    };
    emailjs
      .sendForm(
        "service_wjq59mw",
        "template_z9396ue",
        ref?.current,
        "v-at_riI5YnMFS7bL",
        templateParams
      )
      .then(
        (result) => {
          if (result?.text === "OK") {
            toast({
              title: "Email sent successfully",
              description: "Mail was successfully sent ",
              status: "success",
              duration: 5000,
              isClosable: true,
              position: "top-right",
            });
          }
        },
        (error) => {
          toast({
            title: "Email could not be sent",
            description: "sorry email could not be sent ",
            status: "error",
            duration: 5000,
            isClosable: true,
            position: "top-right",
          });
        }
      );
  };
  return (
    <Flex w="full" h="full" direction="column" scrollBehavior="smooth">
      <Navbar />
      <Banner image={bannerImages[0]} minH="100vh">
        <Flex
          direction="column"
          justify="center"
          align="center"
          color="white"
          scrollBehavior="smooth"
        >
          <Text fontSize="4rem" fontWeight="extrabold">
            Kathmandu
          </Text>
          <Text fontSize="1.5rem">Our Headquarter</Text>

          <VStack
            spacing={2}
            mt="2rem"
            direction="column"
            justify="center"
            align="center"
            fontSize="1.3rem"
            fontWeight="medium"
          >
            <Text> 198 West 21th Street, Suite 721</Text>
            <Text> New York NY 10010 </Text>
            <Text> Phone: +88 (0) 101 0000 000 </Text>
            <Text> Email: voyage@qodeinteractive.com </Text>
          </VStack>
          <Flex mt="2rem">
            {" "}
            {socialLinks?.map((el, i) => (
              <Flex key={i} margin="1rem" cursor="pointer">
                {" "}
                {el?.icon}{" "}
              </Flex>
            ))}{" "}
          </Flex>
          <Flex>
            <Button
              onClick={scrollToContactForm}
              bg="tomato"
              mt="2rem"
              borderRadius={0}
              p={"1.5rem"}
              _hover={{
                opacity: 0.8,
              }}
            >
              Contact us
            </Button>
            <Button
              onClick={scrollToMap}
              bg="blue.300"
              mt="2rem"
              borderRadius={0}
              ml="1rem"
              p={"1.5rem"}
              _hover={{
                opacity: 0.8,
              }}
            >
              Our location
            </Button>
          </Flex>
        </Flex>
      </Banner>
      <Flex w="full" justify="center" align="center" id="contact_form">
        <Flex
          w="90%"
          h="full"
          justify="center"
          align="center"
          direction="column"
        >
          <Flex
            flexDirection="column"
            w={{ base: "full", lg: "50%" }}
            mt="5rem"
            h="fit-content"
            mb="6rem"
          >
            <form ref={ref}>
              <HStack spacing={5} marginBottom="2rem" w="full" h="full">
                <FormControl isRequired>
                  <InputGroup>
                    <Input
                      type="text"
                      name="name"
                      placeholder="Your Name"
                      h="3.4rem"
                    />
                  </InputGroup>
                </FormControl>

                <FormControl isRequired>
                  <InputGroup>
                    <Input
                      type="email"
                      name="email"
                      placeholder="Your Email"
                      h="3.4rem"
                    />
                  </InputGroup>
                </FormControl>
              </HStack>

              <FormControl isRequired>
                <Textarea
                  name="message"
                  placeholder="Your Message"
                  rows={6}
                  resize="none"
                />
              </FormControl>
              <FormControl
                w="full"
                display="flex"
                alignItems="center"
                justifyContent="start"
              >
                <Button mt="1rem" p="1.5rem" bg="tomato" onClick={sendEmail}>
                  Submit
                </Button>
              </FormControl>
            </form>
          </Flex>
          <div
            id="location"
            className="w-full bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10  h-[30rem] flex items-center  relative m-10 justify-center"
          >
            <iframe
              width="100%"
              height="100%"
              className="absolute inset-0"
              frameBorder="0"
              title="map"
              marginHeight="0"
              marginWidth="0"
              scrolling="no"
              src="https://maps.google.com/maps?width=100%&height=600&hl=en&q=nxplore&ie=UTF8&t=&z=14&iwloc=B&output=embed"
            ></iframe>
          </div>
        </Flex>
      </Flex>
      <Footer />
    </Flex>
  );
}
