import {
  Container,
  Flex,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from "@chakra-ui/react";
import React, { useState } from "react";
import Banner from "../../components/Banner";
import { bannerImages } from "../../constants/Images";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer";
import Itenary from "../../components/Itenary";
import OverView from "../../components/OverView";

const PlaceId = () => {
  const placeData = {
    overView: [
      "A remarkable journey into the Himalayas, the Annapurna Base Camp Trek heads to the base camp of the tenth highest mountain in the world- Mt. Annapurna (8091m). This trip is a glorious adventure that entails thrilling trek across the Annapurna Conservation Area. The ABC Trek also explores the peaceful Gurung settlements to the south of the Annapurna range. Moreover, the famous landmarks across the trekking trail to ABC make the trek even more surreal",
      "The ABC Trek begins as you land at the Tribhuvan International Airport in Kathmandu. Then, a drove across the Prithvi Highway takes you to the shadow of Mt. Machhapuchhre (6997m) to Pokhara. It is a popular tourist getaway and houses various attractions, including adventure sports like paragliding and zip-lining. Another short drive from Pokhara brings you to Nayapul from where the trek begins. Passing Ulleri village, the trek reaches Ghorepani village. Poonhill viewpoint lies to the west of the village and is a remarkable location to enjoy the panorama of the Annapurna range. From here, you head north to Chhomrong and follow the Chhomrong River. The trek goes past Dovan and Hinku Cave and reaches the Machhapuchhre Base Camp. It is a famous destination on the ABC trail, and trekkers can explore around before heading to the Annapurna Base Camp.",
      "The base camp offers a grand view of the Annapurna and Dhaulagiri ranges along with the Nilgiri Himalayas. After enjoying the scenery of the Himalayas, you begin to descend to Nayapul. En route, you also make a stop at the popular Jhinu Danda. A short drive followed by a longer one takes you back to the capital. It is a trek with moderate difficulty, and those with average physical fitness can do the trek. Also, the spring and autumn seasons are the ideal time to do the trek. At Narba Treks, we have an excellent team of skilled and experienced guides and porters.Moreover, we also have various trip packages with lucrative offers across the country.Customization and modification of the itinerary are always welcome.",
    ],
    itenery: {
      1: {
        desc: `Welcome to Nepal! One of the company representatives will receive you at Tribhuvan
International Airport. He/she will drive you across the capital and take you to your hotel. Youcan also prepare for the trek as you explore the valley for the rest of the day.`,
        length: null,
        time: null,
      },
      2: {
        desc: `On this day, you will get on a vehicle to traverse across the Prithvi Highway and head to
Pokhara. The views of the Annapurna range and Mt. Machhapuchhre (6997m) will offer a glimpse of what&#39;s to come next on the trip. Pokhara is also known as the City of Lakes and houses numerous attractions and adventure sports`,
        length: 850,
        time: 7,
      },
      3: {
        desc: `On this day, you will get on a short drive to the west of Pokhara and reach Nayapul. The trek officially begins from here and ascends to Ulleri. The trail sees you ascend around 900 meters through several villages and forested paths. As you climb uphill via the great stone steps of Ulleri, you will reach you stop for the day.`,

        length: 1970,
        time: 5.5,
      },
      4: {
        desc: `Leaving Ulleri, you will ascend through the dense forests of rhododendron. This forest is one of the biggest ones in the entire nation and houses a wide variety of wildlife. En route, you will also visit a few Magar villages on the way to Ghorepani. It is a Gurung and Magar village which offers a mesmerizing display of local culture and lifestyle.`,
        length: 2850,
        time: 4,
      },
    },
  };
  const tabs = Object.keys(placeData);
  const [activeTabs, setActiveTabs] = useState(1);
  return (
    <Flex direction="column">
      <Navbar />
      <Banner image={bannerImages[0]} minH="80vh" topic={"Annapurna"} />
      <Flex
        w="full"
        h="full"
        dir="column"
        justify="center"
        align="center"
        p={{ lg: "10", base: "3" }}
      >
        <Flex
          w={{ base: "100%", lg: "80%" }}
          h="full"
          dir="column"
          justify="start"
          align="start"
        >
          <Tabs variant="soft-rounded" colorScheme="green">
            <TabList>
              {tabs?.map((el, i) => (
                <Tab key={i} onClick={() => setActiveTabs(i + 1)}>
                  {" "}
                  {el}{" "}
                </Tab>
              ))}
            </TabList>
            <TabPanels>
              {activeTabs === 2 && <Itenary data={placeData["itenery"]} />}
              {activeTabs === 1 && <OverView overView={placeData?.overView} />}
            </TabPanels>
          </Tabs>
        </Flex>
      </Flex>
      <Footer />
    </Flex>
  );
};

export default PlaceId;
