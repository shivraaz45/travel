import { Flex } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import Banner from "../../components/Banner";
import BlogCard from "../../components/BlogCard";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar/Navbar";
import { bannerImages } from "../../constants/Images";

const Blogs = () => {
  const items = new Array(6).fill("shiv");

  return (
    <Flex w="full" h="100%" direction="column">
      <Navbar />
      <Banner image={bannerImages[0]} topic="Blogs" />
      <Flex w="full" h="100%" justify="center" align="center" py="3rem">
        <Flex
          bg="white !important"
          w={{ md: "90%", base: "full", lg: "75%" }}
          justify={{ base: "center", lg: "start" }}
          align="center"
          direction="column"
        >
          {items?.map((el, i) => (
            <Link key={i} href={`/blogs/${i}`}>
              <a>
                {" "}
                <BlogCard />{" "}
              </a>
            </Link>
          ))}
        </Flex>
      </Flex>
      <Footer />
    </Flex>
  );
};

export default Blogs;
