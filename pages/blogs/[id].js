import { Box, Center, Divider, Flex, Text, VStack } from "@chakra-ui/react";
import React from "react";
import Banner from "../../components/Banner";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar/Navbar";
import ReccomendedBlogCard from "../../components/ReccomendedBlogCard";
import { bannerImages } from "../../constants/Images";

const Blogs = () => {
  return (
    <Flex w="full" h="full" direction="column" align="center">
      <Navbar />
      <Banner image={bannerImages[0]} topic="Blogs" />
      <Flex w="full" h="100%" justify="center" align="center" py="3rem">
        <Flex
          bg="white !important"
          w="75%"
          justify="start"
          align="center"
          direction="column"
        >
          <VStack>
            <Flex w="full" justify="start" direction="column">
              {" "}
              <Text fontSize="2.3rem" fontWeight="semibold" color="gray.800">
                A week in Mustang
              </Text>
              <Text fontWeight="bold" color="gray.500">
                19th sep 2033
              </Text>
              <Text fontWeight="bold" color="gray.600">
                Saroj
              </Text>
            </Flex>

            <Box fontSize="1.3rem">
              <Box mt="1rem">
                {" "}
                Duis dolor est, tincidunt vel enim sit amet, venenatis euismod
                neque. Duis eleifend ligula id tortor finibus faucibus. Donec et
                quam pulvinar, blandit tortor a, sollicitudin mauris. Donec orci
                enim, bibendum a augue quis, aliquet cursus quam. Pellentesque
                pulvinar, elit at condimentum dictum, sapien nibh auctor tortor,
                vel vulputate sapien tortor et velit. Sed nulla nisi, congue eu
                quam vel, molestie gravida ipsum. Curabitur ut lacus vitae
                tellus lacinia pretium. Proin vestibulum sollicitudin tortor,
                quis auctor mi rutrum non. Donec non eros eget purus lobortis
                imperdiet ac vitae est. Interdum et malesuada fames ac ante
                ipsum primis in
              </Box>{" "}
              faucibus.
              <Box mt="2rem">
                Curabitur scelerisque ex at semper molestie. Fusce fringilla
                volutpat purus iaculis convallis. Fusce cursus felis sit amet
                justo suscipit, nec convallis magna porttitor. Nam sed lobortis
                ante, sit amet mattis purus. Nunc tincidunt mollis felis, sed
                bibendum ligula auctor et. Etiam a erat sit amet augue tincidunt
                euismod. Mauris non pulvinar nisi, pellentesque tincidunt
                turpis. Aliquam nec blandit turpis. In hac habitasse platea
                dictumst.
              </Box>
              <Box mt="2rem">
                Donec ut nisi a libero tempus sagittis. Nulla tempor orci et
                nisi fringilla facilisis. Fusce molestie eros nisi, et pulvinar
                risus luctus vitae. Curabitur viverra, dolor ut rhoncus
                hendrerit, tellus massa sagittis nisl, eu condimentum massa sem
                et risus. Quisque varius vel lectus sit amet imperdiet. Maecenas
                sed lectus eu odio fermentum bibendum.
              </Box>
              <Box mt="2rem">
                {" "}
                Aliquam laoreet eros elit, at cursus felis condimentum id.
                Nullam facilisis sapien massa, in ullamcorper nibh tempus eu.
                Quisque eu nisl dui. Nullam ac elementum libero.
              </Box>
            </Box>
          </VStack>
        </Flex>
      </Flex>
      <VStack w="90%">
        <Flex justify="start" w="full">
          {" "}
          <Text fontSize="2rem" color="gray.500" fontWeight="semibold">
            Recomended Blogs
          </Text>
        </Flex>
        <Divider />
        <Flex w="full" wrap="wrap" justify="start">
          {new Array(3).fill("s").map((el, i) => (
            <ReccomendedBlogCard key={i} />
          ))}
        </Flex>
      </VStack>
      <Footer />
    </Flex>
  );
};

export default Blogs;
