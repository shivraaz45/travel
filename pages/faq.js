import { Box, Center, Container, Flex, Text } from "@chakra-ui/react";
import React from "react";
import Banner from "../components/Banner";
import FAQAccordin from "../components/FAQAccordin";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar/Navbar";
import QuoteCard from "../components/QuoteCard";
import { bannerImages } from "../constants/Images";
const faq = () => {
  const FAQs = [
    {
      question: "Where will I go?",
      answer:
        "Nullam ac justo efficitur, tristique ligula a, pellentesque ipsum. Quisque augue ipsum, vehicula et tellus nec, maximus viverra metus. Nullam elementum nibh nec pellentesque finibus. Suspendisse laoreet velit at eros eleifend, a pellentesque urna ornare. In sed viverra dui. Donec ultricies, turpis a sagittis suscipit, ex odio volutpat sem, vel molestie ligula enim varius est.",
    },
  ];
  const items = new Array(10).fill(FAQs[0]);

  return (
    <Flex w="full" h="100%" direction="column">
      <Navbar />
      <Banner image={bannerImages[0]} topic="FAQ" />
      <Flex
        w="full"
        h="100%"
        justify="center"
        align="center"
        py="3rem"
        flexDirection={{ base: "column" }}
      >
        {" "}
        <Flex
          bg="white !important"
          w={{ base: "full", md: "full", lg: "80%" }}
          justify="center"
          align="center"
          flexDirection={{ base: "column" }}
        >
          <Flex
            direction="column"
            bg="white !important"
            w="100%"
            justify="start"
            align="start"
          >
            <Text fontSize={"3rem"} fontWeight="extrabold">
              FAQ
            </Text>
            <Box
              w={{ base: "full", md: "full", lg: "50%" }}
              justifyContent={"start"}
              align="start"
              mt={"3rem"}
            >
              {items?.map((item, i) => (
                <FAQAccordin item={item} key={i} />
              ))}
            </Box>
          </Flex>
        </Flex>
      </Flex>
      <Footer />
    </Flex>
  );
};

export default faq;
