import {
  Box,
  Center,
  Container,
  Flex,
  keyframes,
  ScaleFade,
  useMediaQuery,
} from "@chakra-ui/react";
import Head from "next/head";
import Image from "next/image";
import Navbar from "../components/Navbar/Navbar";
import styles from "../styles/Home.module.css";
import "swiper/css";
// import Swiper core and required modules
import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  EffectFade,
  Autoplay,
} from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/effect-fade";
import { bannerImages, homeBanner } from "../constants/Images";
import { AnimatePresence, m } from "framer-motion";
import Slider from "../components/Slider";
import MostPopularTour from "../components/MostPopularTour";
import WonderFulPlaces from "../components/WonderFulPlaces";
import Footer from "../components/Footer";
import Blogsection from "../components/Blogsection";
import { motion, useScroll, useTransform } from "framer-motion";
import WhyChooseusSection from "../components/WhyChooseusSection";
export default function Home() {
  const [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");
  console.log(isLargerThan1280);
  const { scrollY } = useScroll();

  return (
    <div className={styles.container}>
      <Navbar />
      <Center className="h-screen w-full bg-red-400 flex justify-center items-center">
        <Swiper
          className="w-full h-full center bg-green-600  "
          // install Swiper modules
          spaceBetween={50}
          slidesPerView={1}
          centeredSlides={true}
          effect="fade"
          loop={true}
          autoplay={{
            delay: 10 * 1000,
            disableOnInteraction: false,
          }}
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log("slide change")}
          modules={[Autoplay, EffectFade, Navigation, Pagination]}
        >
          {homeBanner.map((image, i) => (
            <SwiperSlide className="bg-red-500 " key={i}>
              <Slider i={i} image={image} />
            </SwiperSlide>
          ))}
          ...
        </Swiper>
      </Center>
      <MostPopularTour />
      <WonderFulPlaces />
      <WhyChooseusSection />
      <Blogsection />/
      <Footer />
    </div>
  );
}
