import { Box, Center, Flex, Text } from "@chakra-ui/react";
import Banner from "../components/Banner";
import DestinationContainer from "../components/DestinationContainer";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar/Navbar";
import { bannerImages } from "../constants/Images";

const Destinations = () => {
  return (
    <div>
      <Navbar />
      <Banner image={bannerImages[0]} topic="Destinations" />
      <Flex
        justify="center"
        align="center"
        direction="column"
        p={{ base: ".2rem", md: ".6rem", lg: "2rem" }}
        pt={{ base: "1rem", md: "1.3rem", lg: "2rem" }}
        bg="white !important"
      >
        {" "}
        <Text
          align="center"
          fontSize={{ base: "1rem", md: "2rem", lg: "3rem" }}
        >
          {" "}
          Choose Your Destination
        </Text>
        <Center flexDirection="column" color="gray.500">
          {" "}
          <Box textAlign="center" mt="1rem">
            loremEst eiusmod culpa ipsum ex exercitation cillum
          </Box>
          <Box mt=".5rem" textAlign="center">
            {" "}
            elit anim commodo duis. Ipsum incididunt qui proident culpa dolore
            commodo minim labore nulla ea amet nostrud.{" "}
          </Box>{" "}
        </Center>
        <DestinationContainer />
      </Flex>
      <Footer />
    </div>
  );
};

export default Destinations;
