import {
  Box,
  Center,
  Container,
  Divider,
  Flex,
  SimpleGrid,
  Text,
  useMediaQuery,
} from "@chakra-ui/react";
import React from "react";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import { bannerImages } from "../constants/Images";
import Navbar from "../components/Navbar/Navbar";
import Image from "next/image";
import DestinationCard from "../components/DestinationCard";
import ReccomendedCard from "../components/ReccomendedCard";
import TeamCard from "../components/TeamCard";
import OurTeamSection from "../components/OurTeamSection";
import MeetOurTeam from "../components/MeetOurTeam";
const Aboutus = () => {
  const reccomendedItems = new Array(4).fill("shiv");
  const [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");

  return (
    <Flex w="full" h="100%" direction="column">
      <Navbar />
      <Banner image={bannerImages[0]} topic="About US" />
      <Flex
        justify="center"
        align="center"
        direction="column"
        py={{ base: ".5rem", md: "1rem", lg: "3rem" }}
        bg="white !important"
      >
        {" "}
        <Center flexDirection="column" color="gray.500">
          <Text fontSize="3rem"> Our Story</Text>
          <SimpleGrid
            w={["full", "full", "80%"]}
            columns={isLargerThan1280 ? 2 : 1}
            spacing="20px"
            position="relative"
            p={{ base: ".4rem", md: "1rem", lg: "1rem" }}
          >
            <Flex direction="column">
              <Box w="full" wordBreak="break-all">
                <Box
                  mt="1.5rem"
                  className="first-letter:text-teal-200 first-letter:text-4xl"
                >
                  {" "}
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
                  sollicitudin, telluclass vitae condimentum egestas, libero
                  dolor auctor tellus, eu consectetur neque elit quis nunc. Cras
                  elementum pretium est. Nullam ac justo efficitur, tristique
                  ligula a, pellentesque ipsum. Quisque augue ipsum, vehicula et
                  tellus nec, maximus viverra metus. Nullam elementum nibh nec
                  pellentesque finibus. Suspendisse laoreet velit at eros
                  eleifend, a pellentesque urna ornare. In sed viverra dui. Duis
                  ultricies mi sed lorem blandit, non sodales sapien fermentum.
                  Pellentesque sodales ipsum nisi.
                </Box>
                <Box mt="1.5rem">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
                  sollicitudin, tellus vitae condimentum egestas, libero dolor
                  auctor tellus, eu consectetur neque elit quis nunc. Cras
                  elementum pretium est. Nullam ac justo efficitur, tristique
                  ligula a, pellentesque ipsum. Quisque augue ipsum, vehicula et
                  tellus nec, maximus viverra metus. Lorem ipsum dolor sit amet,
                  consectetur adipiscing elit. Cras sollicitudin, tellus vitae
                  condimentum egestas, libero dolor auctor tellus, eu
                  consectetur neque elit quis nunc.
                </Box>

                <Box mt="1.5rem">
                  Cras elementum pretium est. Nullam ac justo efficitur,
                  tristique ligula a, pellentesque ipsum. Quisque augue ipsum,
                  vehicula et tellus nec, maximus viverra metus. Nullam
                  elementum nibh nec pellentesque finibus. Suspendisse laoreet
                  velit at eros eleifend, a pellentesque urna ornare. In sed
                  viverra dui. Duis ultricies mi sed lorem blandit, non sodales
                  sapien fermentum. Suspendisse ultrices nulla eu volutpat
                  volutpat.
                </Box>
              </Box>
            </Flex>
            <Flex w="full" h="full" position="relative">
              <Image src={bannerImages[0]} layout="fill" alt="About us" />
            </Flex>
          </SimpleGrid>
        </Center>
      </Flex>
      {/* <OurTeamSection /> */}
      <Flex w="full" justify="center">
        <Flex
          direction="column"
          align="center"
          w={{ base: "100%", md: "100%", lg: "80%" }}
          p={{ base: ".4rem", md: "1rem", lg: "1rem" }}
          justify="center"
        >
          <MeetOurTeam />
          <Flex w="full" justify="start">
            <Text fontSize={30} fontWeight="extrabold">
              Reccomended
            </Text>
          </Flex>
          <Divider mb="2rem" mt="1rem" />
          <SimpleGrid
            w={["full", "full", "100%"]}
            columns={[1, 2, 4]}
            row={[2, null, 5]}
            spacing="10px"
          >
            {reccomendedItems?.map((el, i) => (
              <ReccomendedCard key={i} />
            ))}
          </SimpleGrid>
        </Flex>
      </Flex>
      {/* <TeamCard /> */}
      <Footer />
    </Flex>
  );
};

export default Aboutus;
